package com.reznik.spring.lesson.security.rest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class MainController {

    @GetMapping("/")
    public String homePage() {
        return "home";
    }

    @GetMapping("/authenticated")
    public String pageForAuthenticatedUsers(Principal principal) {
        return "secured part of web service " + principal.getName();
    }

    @GetMapping("/read-profile")
    public String pageForReadProfile() {
        return "read profile page";
    }

    @GetMapping("/only-for-admins")
    public String pageForOnlyAdmins() {
        return "admins page";
    }

    @GetMapping("/test-has-authority")
    @PreAuthorize("hasAuthority('READ_PROFILE')")
    public String testHasAuthority() {
        return "admins page";
    }
}
